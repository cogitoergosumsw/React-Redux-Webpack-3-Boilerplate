# React-Redux-Webpack-3-Boilerplate
React Redux using Webpack 3 Boilerplate

This boilerplate runs on,

1. React@15.6.2
2. Redux@3.0.4
3. React-Redux@5.0.6
4. Webpack@3.10.0
5. Webpack-dev-server@2.11.1
6. Babel@6.26.0

Run npm install to install all the necessary dependencies.

Run npm start to start development server at localhost:8080

Run npm run build to build the web app in Production environment (and get bundle.js)
